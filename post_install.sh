#!/bin/bash

install_aurman(){
    pwd="$(pwd)"
    mkdir /tmp/aurman_download
    cd /tmp/aurman_download
    git clone https://aur.archlinux.org/aurman.git
    cd ./aurman
    makepkg -si
    cd "$pwd"

}

PACKAGES=()
PACKAGES=("${PACKAGES[@]}"
          # basic development packages
          "base-devel"
          # editors
          "emacs" "vim" "git"
          # internet
          "firefox" "filezilla" "transmission-cli"
          # communications
          "mumble" "hexchat" "thunderbird"
          # networking
          "nmap" "mtr" "wireshark-qt" "aircrack-ng" "bmon" "tcpdump" "nm-connection-editor" "openssh"
          # useful
          "redshift" "screenfetch" "unzip"  "graphviz" "keepassxc" "htop" "rsync" "virtualbox" "virtualbox-host-modules-arch"
          # writing
          "texlive-most" "texlive-bin" "biber" "pandoc"
          # yubikey
          "yubikey-manager" "yubikey-manager-qt" "yubikey-personalization" "yubikey-personalization-gui"
          # fonts
          "powerline-fonts"
         )

LAPTOP_PACKAGES=("${PACKAGES[@]}"
                 "tlp"
                )

sudo pacman -Syu
install_aurman

aurman -S --needed ${PACKAGES[@]}
