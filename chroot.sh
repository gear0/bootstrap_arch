#!/bin/bash
read -s -p "Enter hostname: " HOSTNAME
read -s -p "Enter username: " USERNAME
read -s -p "Enter user password: " password
printf "setting up timezone...\n"
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc

printf "setting locle...\n"
sed -i 's/^#\(en_US\.UTF-8 UTF-8\)/\1/' /etc/locale.gen
locale-gen
printf "KEYMAP=de-latin1" > /etc/vconsole.conf


printf "setting up network...\n"
pacman -S --noconfirm networkmanager
systemctl enable NetworkManager.service
systemctl start NetworkManager.service
printf "$HOSTNAME" > /etc/hostname


printf "configure mkinitcpio..."
sed -i 's/^HOOKS.*/HOOKS=\(base udev autodetect keyboard keymap consolefont modconf block encrypt lvm2 filesystems fsck\)/' /etc/mkinitcpio.conf

mkinitcpio -p linux

pacman -S syslinux intel-ucode --noconfirm
syslinux-install_update -i -a -m
sed -i 's/APPEND.*/APPEND cryptdevice=\/dev\/sda2:cryptlvm root=\/dev\/mapper\/MainVG-root rw lang=de locale=de_DE.UTF-8/' /boot/syslinux/syslinux.cfg
printf "installing additional packages...\n"
pacman -S --noconfirm wget curl rxvt-unicode xorg xorg-xinit i3-wm i3status i3lock borg zsh sudo nfs-utils cifs-utils python-llfuse dmenu base-devel

printf "adding user...\n"
useradd --uid 1000 --user-group --create-home --groups wheel --home-dir /home/$USERNAME --shell /usr/bin/zsh $USERNAME
printf "$password\n$password\n" | passwd $USERNAME
echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers.d/00_allow_wheel

# additional packages
# htop mtr screenfetch unzip nmap firefox wireshark-qt
